//
//  main.m
//  BLE4.0-SerialPort
//
//  Created by lan on 15/9/16.
//  Copyright © 2015年 lan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
