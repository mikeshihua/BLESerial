//
//  AppDelegate.h
//  BLE4.0-SerialPort
//
//  Created by lan on 15/9/16.
//  Copyright © 2015年 lan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreBluetooth/CBService.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

