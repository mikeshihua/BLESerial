//
//  BLESettingArrowItem.h
//  BLE4.0-SerialPort
//
//  Created by lan on 15/9/22.
//  Copyright © 2015年 lan. All rights reserved.
//

#import "BLESettingItem.h"

@interface BLESettingArrowItem : BLESettingItem

// 跳转的控制器的类名
@property (nonatomic, strong) Class destVcClass;

+ (instancetype)itemWithIcon:(NSString *)icon title:(NSString *)title destVcClass:(Class)destVcClass;

@end
