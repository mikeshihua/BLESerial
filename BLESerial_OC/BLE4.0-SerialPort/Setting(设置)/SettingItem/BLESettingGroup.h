//
//  BLESettingGroup.h
//  BLE4.0-SerialPort
//
//  Created by lan on 15/9/22.
//  Copyright © 2015年 lan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLESettingGroup : NSObject

@property (nonatomic, copy)NSString *header;
@property (nonatomic, strong)NSArray *items;
@property (nonatomic, copy)NSString *footer;

@end
