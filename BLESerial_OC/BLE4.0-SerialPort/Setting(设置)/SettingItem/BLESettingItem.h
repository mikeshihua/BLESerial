//
//  BLESettingItem.h
//  BLE4.0-SerialPort
//
//  Created by lan on 15/9/22.
//  Copyright © 2015年 lan. All rights reserved.
//  设置选项模型

#import <Foundation/Foundation.h>
typedef void(^BLESettingItemOption)();

@interface BLESettingItem : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *icon;

+ (instancetype)itemWithIcon:(NSString *)icon title:(NSString *)title;

@property (nonatomic, copy)BLESettingItemOption option;

@end
