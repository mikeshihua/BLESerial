//
//  BLESettingItem.m
//  BLE4.0-SerialPort
//
//  Created by lan on 15/9/22.
//  Copyright © 2015年 lan. All rights reserved.
//

#import "BLESettingItem.h"

@implementation BLESettingItem

+ (instancetype)itemWithIcon:(NSString *)icon title:(NSString *)title {
    BLESettingItem *item = [[self alloc] init];
    
    item.icon = icon;
    item.title = title;
    
    return item;
}

@end
