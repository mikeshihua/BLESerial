//
//  BLESettingTableViewController.m
//  BLE4.0-SerialPort
//
//  Created by lan on 15/9/22.
//  Copyright © 2015年 lan. All rights reserved.
//

#import "BLESettingTableViewController.h"
#import "BLESettingItem.h"
#import "BLESettingGroup.h"
#import "BLESettingCell.h"

#import "BLESettingArrowItem.h"
#import "BLESettingSwitchItem.h"

#import "BLEConnectViewController.h"

@interface BLESettingTableViewController ()

@property (nonatomic, strong) NSMutableArray *dataList;

@end

@implementation BLESettingTableViewController

- (NSMutableArray *)dataList {
    if ( _dataList == nil) {
        _dataList = [NSMutableArray array];
        
        
        // 0组
        BLESettingArrowItem *connect = [BLESettingArrowItem itemWithIcon:@"iconfont-diary" title:@"连接" destVcClass:[BLEConnectViewController class]];
        
        BLESettingItem *disconnect = [BLESettingItem itemWithIcon:@"iconfont-diary" title:@"断开"];
        disconnect.option = ^ {
            
            [self.navigationController popViewControllerAnimated:YES];
            
            if ([self.delegate respondsToSelector:@selector(settingTableViewContrllerDisconnectBtnClick:)]) {
                [self.delegate settingTableViewContrllerDisconnectBtnClick:self];
            }
            
        };
        
        BLESettingItem *clean = [BLESettingItem itemWithIcon:@"iconfont-diary" title:@"清除"];
        clean.option = ^ {
        
            [self.navigationController popViewControllerAnimated:YES];
            
            if ([self.delegate respondsToSelector:@selector(settingTableViewContrllerCleanBtnClick:)]) {
                
                [self.delegate settingTableViewContrllerCleanBtnClick:self];
            }
            
        
        };
        
        BLESettingGroup *group0 = [[BLESettingGroup alloc] init];
        group0.header = @"设置";
        group0.items = @[connect, disconnect, clean];
        
        
        // 1组
        BLESettingItem *help = [BLESettingItem itemWithIcon:@"iconfont-diary" title:@"帮助"];
        BLESettingItem *about = [BLESettingItem itemWithIcon:@"iconfont-diary" title:@"关于"];
        BLESettingSwitchItem *test = [BLESettingSwitchItem itemWithIcon:@"'" title:@"test"];
        
        BLESettingGroup *group1 = [[BLESettingGroup alloc] init];
        group1.header = @"帮助";
        group1.items = @[help, about, test];
    
        [_dataList addObject:group0];
        [_dataList addObject:group1];
        
    }
    return _dataList;
}

- (instancetype)init
{
    return [super initWithStyle:UITableViewStyleGrouped];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return self.dataList.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    BLESettingGroup *group = self.dataList[section];
    return group.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    // 创建cell
    BLESettingCell *cell = [BLESettingCell cellWithTableView:tableView];
    
    // 取出模型
    BLESettingGroup *group = self.dataList[indexPath.section];
    BLESettingItem *item =  group.items[indexPath.row];

    // 传递模型
    cell.item = item;
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    BLESettingGroup *group = self.dataList[section];
    return group.header;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // 取出模型
    BLESettingGroup *group = self.dataList[indexPath.section];
    BLESettingItem *item =  group.items[indexPath.row];
    
    // 执行block 代码
    if (item.option) {
        item.option();
        return;
    }
    
    // 执行箭头样式的代码
    if ([item isKindOfClass:[BLESettingArrowItem class]]) {
        BLESettingArrowItem *arrowItem = (BLESettingArrowItem *)item;
        
        if (arrowItem.destVcClass) {
            UIViewController *vc = [[arrowItem.destVcClass alloc] init];
            vc.title = item.title;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}


@end
