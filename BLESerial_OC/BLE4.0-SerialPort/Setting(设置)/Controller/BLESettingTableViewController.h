//
//  BLESettingTableViewController.h
//  BLE4.0-SerialPort
//
//  Created by lan on 15/9/22.
//  Copyright © 2015年 lan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BLESettingTableViewController;
@protocol BLESettingTableViewControllerDelegate <NSObject>

@optional

- (void)settingTableViewContrllerCleanBtnClick:(BLESettingTableViewController *)settingVC;

- (void)settingTableViewContrllerDisconnectBtnClick:(BLESettingTableViewController *)settingVC;

@end

@interface BLESettingTableViewController : UITableViewController

@property (nonatomic, weak)id<BLESettingTableViewControllerDelegate> delegate;

@end
