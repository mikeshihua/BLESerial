//
//  BLESettingCell.m
//  BLE4.0-SerialPort
//
//  Created by lan on 15/9/22.
//  Copyright © 2015年 lan. All rights reserved.
//

#import "BLESettingCell.h"
#import "BLESettingItem.h"

#import "BLESettingArrowItem.h"
#import "BLESettingSwitchItem.h"

@interface BLESettingCell ()

@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UISwitch *switchView;
@property (nonatomic, strong) UILabel *labelView;

@end

@implementation BLESettingCell


- (UISwitch *)switchView {
    if ( _switchView == nil) {
        _switchView = [[UISwitch alloc] init];
    }
    return _switchView;
}


- (void)setItem:(BLESettingItem *)item {
    _item = item;
    
    // 设置数据
    [self setUpData];

    // 设置样式
    [self setUpAccessoryView];
    
}

// 设置样式
- (void)setUpAccessoryView {
    if ([self.item isKindOfClass:[BLESettingArrowItem class]]) {  // 箭头
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        // 注意：一定要加
        self.selectionStyle = UITableViewCellSelectionStyleDefault;
        
    } else if([self.item isKindOfClass:[BLESettingSwitchItem class]]) {  // 开关
        self.accessoryView = self.switchView;
        self.selectionStyle = UITableViewCellSelectionStyleNone; // 取消可选中样式，并非不可选中
        
    } else {
        self.accessoryView = nil;
        self.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
}

// 设置数据
- (void)setUpData {
    self.imageView.image = [UIImage imageNamed:self.item.icon];
    self.textLabel.text = self.item.title;
}


+ (instancetype)cellWithTableView:(UITableView *)tableView {
    
    static NSString *ID = @"cell";
    BLESettingCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[BLESettingCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
    }
    return cell;
}


@end
