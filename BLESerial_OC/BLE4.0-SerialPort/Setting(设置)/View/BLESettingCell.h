//
//  BLESettingCell.h
//  BLE4.0-SerialPort
//
//  Created by lan on 15/9/22.
//  Copyright © 2015年 lan. All rights reserved.
//  自定义设置cell

#import <UIKit/UIKit.h>

@class BLESettingItem;
@interface BLESettingCell : UITableViewCell

@property (nonatomic, strong)BLESettingItem *item;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
