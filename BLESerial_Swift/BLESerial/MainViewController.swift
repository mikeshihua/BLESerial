//
//  ViewController.swift
//  BLESerial
//
//  Created by Maxwell Rose on 15/12/7.
//  Copyright © 2015年 LLLTeam. All rights reserved.
//

import UIKit

class MainViewController: UIViewController,UIPopoverPresentationControllerDelegate{
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var connectStatesLabel: UILabel!
    @IBOutlet weak var sendedCounterLabel: UILabel!
    @IBOutlet weak var receivedCounterLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var sentBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.addKeyboardNotification()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeKeyboardNotification()
    }
    
    func addKeyboardNotification() {
        NSNotificationCenter.defaultCenter().addObserverForName(UIKeyboardWillShowNotification, object: nil, queue: NSOperationQueue.mainQueue()) { (notification: NSNotification) -> Void in
            UIView.animateWithDuration(0.99, animations: { () -> Void in
                let userInfo = notification.userInfo
                let framValue = userInfo![UIKeyboardFrameEndUserInfoKey]
                var currentFrame = self.view.frame
                currentFrame.size.height = framValue!.CGRectValue.origin.y
                self.view.frame = currentFrame
            })
            print("show")
        }
        
        NSNotificationCenter.defaultCenter().addObserverForName(UIKeyboardWillHideNotification, object: nil, queue: NSOperationQueue.mainQueue()) { (notification: NSNotification) -> Void in
            UIView.animateWithDuration(0.99, animations: { () -> Void in
                self.view.frame = UIScreen.mainScreen().bounds
            })
            print("hide")

        }
    }
    
    func removeKeyboardNotification() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    //MARK: -
    @IBAction func itemBtnAction(sender: AnyObject) {
        print("item")
    }
    
    @IBAction func settingItemBtnAction(sender: AnyObject) {
        print("setting")

    }
    
    @IBAction func sentBtnAction(sender: AnyObject) {
        
    }

    @IBAction func sentBtnLongPressAction(sender: UILongPressGestureRecognizer) {
        print("long press")
        let popoverContent = (self.storyboard?.instantiateViewControllerWithIdentifier("ATCommand"))! as UIViewController        
        popoverContent.modalPresentationStyle = UIModalPresentationStyle.Popover
        let popover = popoverContent.popoverPresentationController
        popoverContent.preferredContentSize = CGSizeMake(200,350)
        popover!.delegate = self
        popover!.sourceView = self.sentBtn
        self.presentViewController(popoverContent, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }

    @IBAction func switchFormatBtnAction(sender: AnyObject) {
        
    }
    
    @IBAction func swipUpAction(sender: AnyObject) {
        print("swipeUp")
        inputTextField.becomeFirstResponder()
    }
    
    @IBAction func swipDownAction(sender: AnyObject) {
        print("swipeDown")
        inputTextField.resignFirstResponder()
        textView.resignFirstResponder()
    }
    
    
}

